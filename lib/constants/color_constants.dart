import "dart:ui";

import 'package:hexcolor/hexcolor.dart';

final goldenColor = Hexcolor('#DAAD68');
final greenColor = Hexcolor("#187572");
final darkGoldenColor = Hexcolor('#B59374');
final successColor = Hexcolor('#03AC13');
final dangerColor = Hexcolor('#F78589');
final primaryGreen = Hexcolor('#036562');
