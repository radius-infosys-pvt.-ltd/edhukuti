import 'package:flutter/material.dart';

class FundLoadDate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
                      margin: EdgeInsets.only(top:30),
                      height: 70,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey.withOpacity(0.2)),
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(top: 8.0, bottom: 6),
                                padding: const EdgeInsets.symmetric(
                                    vertical: 3.0, horizontal: 8),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.blue),
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Row(
                                  children: [
                                    Text(
                                      'Recurring Date',
                                      style: TextStyle(fontSize: 10),
                                    ),
                                  ],
                                ),
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.calendar_today,
                                    size: 15,
                                    color: Colors.green,
                                  ),
                                  Text(' 1st Oct'),
                                ],
                              )
                            ],
                          ),
                          VerticalDivider(
                            indent: 10,
                            endIndent: 10,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(top: 8.0, bottom: 6),
                                padding: const EdgeInsets.symmetric(
                                    vertical: 3.0, horizontal: 8),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.red),
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Row(
                                  children: [
                                    Text(
                                      'Due Date',
                                      style: TextStyle(fontSize: 10),
                                    ),
                                  ],
                                ),
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.calendar_today,
                                    size: 15,
                                    color: Colors.red,
                                  ),
                                  Text(' 30th Sep'),
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                    );
  }
}