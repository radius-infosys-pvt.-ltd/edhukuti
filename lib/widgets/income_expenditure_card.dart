import 'package:flutter/material.dart';

class IncomeExpenditureCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
                      margin: EdgeInsets.only(top:20),
                      height: 70,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey.withOpacity(0.2)),
                        borderRadius: BorderRadius.circular(15)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(top: 8.0, bottom: 6),
                                padding: const EdgeInsets.symmetric(
                                    vertical: 3.0, horizontal: 8),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.blue),
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Row(
                                  children: [
                                    Text(
                                      'Contribution',
                                      style: TextStyle(fontSize: 10),
                                    ),
                                  ],
                                ),
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.arrow_upward,
                                    size: 15,
                                    color: Colors.green,
                                  ),
                                  Text('\$50,000'),
                                ],
                              )
                            ],
                          ),
                          VerticalDivider(
                            indent: 10,
                            endIndent: 10,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(top: 8.0, bottom: 6),
                                padding: const EdgeInsets.symmetric(
                                    vertical: 3.0, horizontal: 8),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.red),
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Row(
                                  children: [
                                    Text(
                                      'Expenditure',
                                      style: TextStyle(fontSize: 10),
                                    ),
                                  ],
                                ),
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.arrow_downward,
                                    size: 15,
                                    color: Colors.red,
                                  ),
                                  Text('\$ 20,000'),
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                    );
  }
}