import 'package:edhukuti/models/message_model.dart';
import 'package:edhukuti/screens/ChatScreen.dart';
import 'package:edhukuti/screens/Dashboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:hexcolor/hexcolor.dart';

class NotificationSwiper extends StatefulWidget {
  @override
  _NotificationSwiperState createState() => _NotificationSwiperState();
}

class _NotificationSwiperState extends State<NotificationSwiper> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        children: [
          Container(
            height: 70,
            decoration: BoxDecoration(
              color: Hexcolor('#FFFCF5'),
              border: Border(
                bottom: BorderSide(
                  color: Colors.black12,
                  width: 1,
                ),
              ),
            ),
            child: Slidable(
              actionPane: SlidableDrawerActionPane(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    width: 40.0,
                    height: 40.0,
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage('assets/images/profile.jpg'),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10, top: 12),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w300,
                                    ),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: 'Saphal Ghimire',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold)),
                                      TextSpan(
                                          text: ' has liked your profile.'),
                                    ],
                                  ),
                                ),
                              ),
                              Text(
                                '',
                              ),
                              Container(
                                padding: EdgeInsets.all(3),
                                margin: EdgeInsets.only(left: 5, right: 10),
                                decoration: BoxDecoration(
                                    color: Colors.orangeAccent,
                                    borderRadius: BorderRadius.circular(8)),
                                height: 20,
                                width: 40,
                                child: Center(
                                  child: Text('NEW',
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.black87)),
                                ),
                              ),
                            ],
                          ),
                          Text(
                            '8 mins ago',
                            style: TextStyle(fontSize: 10, color: Colors.grey),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              // actions: [
              //   IconSlideAction(
              //     caption: 'Share',
              //     color: Colors.tealAccent,
              //     icon: Icons.archive,
              //     onTap: () {},
              //   ),
              // ],
              secondaryActions: [
                IconSlideAction(
                  color: Colors.red[600],
                  icon: Icons.delete,
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Dashboard()));
                  },
                ),
              ],
            ),
          ),
          Container(
            height: 70,
            decoration: BoxDecoration(
              color: Hexcolor('#FFFCF5'),
              border: Border(
                bottom: BorderSide(
                  color: Colors.black12,
                  width: 1,
                ),
              ),
            ),
            child: Slidable(
              actionPane: SlidableDrawerActionPane(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    width: 40.0,
                    height: 40.0,
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage('assets/images/profile.jpg'),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10, top: 12),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w300,
                                    ),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: 'Saphal Ghimire',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold)),
                                      TextSpan(
                                          text:
                                              ' has sent you a friend request.'),
                                    ],
                                  ),
                                ),
                              ),
                              Text(
                                '',
                              ),
                              Container(
                                padding: EdgeInsets.all(3),
                                margin: EdgeInsets.only(left: 5, right: 10),
                                decoration: BoxDecoration(
                                    color: Colors.orangeAccent,
                                    borderRadius: BorderRadius.circular(8)),
                                height: 20,
                                width: 40,
                                child: Center(
                                  child: Text('NEW',
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.black87)),
                                ),
                              ),
                            ],
                          ),
                          Text(
                            '8 mins ago',
                            style: TextStyle(fontSize: 10, color: Colors.grey),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              // actions: [
              //   IconSlideAction(
              //     caption: 'Share',
              //     color: Colors.tealAccent,
              //     icon: Icons.archive,
              //     onTap: () {},
              //   ),
              // ],
              secondaryActions: [
                IconSlideAction(
                  color: Colors.green[600],
                  icon: Icons.assignment_turned_in,
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Dashboard()));
                  },
                ),
                IconSlideAction(
                  color: Colors.red[600],
                  icon: Icons.delete,
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Dashboard()));
                  },
                ),
              ],
            ),
          ),
          Container(
            height: 70,
            decoration: BoxDecoration(
              color: Hexcolor('#FFFCF5'),
              border: Border(
                bottom: BorderSide(
                  color: Colors.black12,
                  width: 1,
                ),
              ),
            ),
            child: Slidable(
              actionPane: SlidableDrawerActionPane(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    width: 40.0,
                    height: 40.0,
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage('assets/images/profile.jpg'),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10, top: 12),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w300,
                                    ),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: 'Saphal Ghimire',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold)),
                                      TextSpan(text: ' has requested to join '),
                                      TextSpan(
                                          text: 'Maharaja Pool.',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold)),
                                    ],
                                  ),
                                ),
                              ),
                              Text(
                                '',
                              ),
                              Container(
                                padding: EdgeInsets.all(3),
                                margin: EdgeInsets.only(left: 5, right: 10),
                                decoration: BoxDecoration(
                                    color: Colors.orangeAccent,
                                    borderRadius: BorderRadius.circular(8)),
                                height: 20,
                                width: 40,
                                child: Center(
                                  child: Text('NEW',
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.black87)),
                                ),
                              ),
                            ],
                          ),
                          Text(
                            '8 mins ago',
                            style: TextStyle(fontSize: 10, color: Colors.grey),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              // actions: [
              //   IconSlideAction(
              //     caption: 'Share',
              //     color: Colors.tealAccent,
              //     icon: Icons.archive,
              //     onTap: () {},
              //   ),
              // ],
              secondaryActions: [
                IconSlideAction(
                  color: Colors.green[600],
                  icon: Icons.assignment_turned_in,
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Dashboard()));
                  },
                ),
                IconSlideAction(
                  color: Colors.red[600],
                  icon: Icons.delete,
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Dashboard()));
                  },
                ),
              ],
            ),
          ),
          Container(
            color: Hexcolor('#E7E7E7'),
            width: double.infinity,
            padding: EdgeInsets.only(left: 15),
            height: 30,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Jan 26',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 70,
            decoration: BoxDecoration(
              color: Hexcolor('#FFFCF5'),
              border: Border(
                bottom: BorderSide(
                  color: Colors.black12,
                  width: 1,
                ),
              ),
            ),
            child: Slidable(
              actionPane: SlidableDrawerActionPane(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    width: 40.0,
                    height: 40.0,
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage('assets/images/profile.jpg'),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10, top: 12),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w300,
                                    ),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: 'Saphal Ghimire',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold)),
                                      TextSpan(text: ' has requested to join '),
                                      TextSpan(
                                          text: 'Maharaja Pool.',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold)),
                                    ],
                                  ),
                                ),
                              ),
                              Text(
                                '',
                              ),
                              Container(
                                padding: EdgeInsets.all(3),
                                margin: EdgeInsets.only(left: 5, right: 10),
                                decoration: BoxDecoration(
                                    color: Colors.orangeAccent,
                                    borderRadius: BorderRadius.circular(8)),
                                height: 20,
                                width: 40,
                                child: Center(
                                  child: Text('NEW',
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.black87)),
                                ),
                              ),
                            ],
                          ),
                          Text(
                            '8 mins ago',
                            style: TextStyle(fontSize: 10, color: Colors.grey),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              // actions: [
              //   IconSlideAction(
              //     caption: 'Share',
              //     color: Colors.tealAccent,
              //     icon: Icons.archive,
              //     onTap: () {},
              //   ),
              // ],
              secondaryActions: [
                IconSlideAction(
                  color: Colors.green[600],
                  icon: Icons.assignment_turned_in,
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Dashboard()));
                  },
                ),
                IconSlideAction(
                  color: Colors.red[600],
                  icon: Icons.delete,
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Dashboard()));
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
