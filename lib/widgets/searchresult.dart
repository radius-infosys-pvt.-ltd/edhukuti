import 'package:flutter/material.dart';

class SearchResult extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Search Result',
              style: TextStyle(
                  color: Theme.of(context).secondaryHeaderColor,
                  fontFamily: 'Philosopher',
                  fontSize: 22,
                  fontWeight: FontWeight.w600),
            ),
           
          ],
        ),
        SizedBox(height: 10,),
        Card(
          elevation: 0.2,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                child: CircleAvatar(
                  backgroundImage: AssetImage(
                      'assets/images/male.png'),
                ),
                padding: EdgeInsets.only(right: 10),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Ramesh Karki',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).accentColor),
                  ),
                  Text(
                    'Verified user',
                    style: TextStyle(fontSize: 10, color: Colors.grey),
                  )
                ],
              ),
              Container(child: Icon(Icons.add_circle_outline,color: Theme.of(context).accentColor,),padding: EdgeInsets.only(left:100),),
            ],
          ),
        ),
         Card(
            elevation: 0.2,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                child: CircleAvatar(
                  backgroundImage: AssetImage('assets/images/Norma avatar.png'),
                  backgroundColor: Theme.of(context).accentColor,
                ),
                padding: EdgeInsets.only(right: 10),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Ramesh Karki',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).accentColor),
                  ),
                  Text(
                    'Verified user',
                    style: TextStyle(fontSize: 10, color: Colors.grey),
                  )
                ],
              ),
              Container(child: Icon(Icons.add_circle_outline,color: Theme.of(context).accentColor,),padding: EdgeInsets.only(left:100),),
            ],
          ),
        ),
         Card(
            elevation: 0.2,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                child: CircleAvatar(
                  backgroundImage: AssetImage(
                      'assets/images/male.png'),
                ),
                padding: EdgeInsets.only(right: 10),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Ramesh Karki',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).accentColor),
                  ),
                  Text(
                    'Verified user',
                    style: TextStyle(fontSize: 10, color: Colors.grey),
                  )
                ],
              ),
              Container(child: Icon(Icons.add_circle_outline,color: Theme.of(context).accentColor,),padding: EdgeInsets.only(left:100),),
            ],
          ),
        )
      ],
    );
  }
}
