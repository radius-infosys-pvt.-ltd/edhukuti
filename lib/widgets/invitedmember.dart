import 'package:edhukuti/screens/invite.dart';
import 'package:flutter/material.dart';

class InvitedMembers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Invited Friends',
              style: TextStyle(
                  color: Theme.of(context).secondaryHeaderColor,
                  fontFamily: 'Philosopher',
                  fontSize: 22,
                  fontWeight: FontWeight.w600),
            ),
            FlatButton(
                onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Invite()));
              },
                child: Text(
                  'Invite a new friend',
                  style: TextStyle(
                      color: Theme.of(context).secondaryHeaderColor, fontSize: 12),
                )),
          ],
        ),
        Card(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                child: CircleAvatar(
                  backgroundImage: AssetImage(
                      'assets/images/male.png'),
                ),
                padding: EdgeInsets.only(right: 10),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Ramesh Karki',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).accentColor),
                  ),
                  Text(
                    'Verified user',
                    style: TextStyle(fontSize: 10, color: Colors.grey),
                  )
                ],
              ),
              Container(child: Icon(Icons.delete,color: Colors.red,),padding: EdgeInsets.only(left:100),),
            ],
          ),
        ),
         Card(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                child: CircleAvatar(
                  backgroundImage: AssetImage('assets/images/Norma avatar.png'),
                  backgroundColor: Theme.of(context).accentColor,
                ),
                padding: EdgeInsets.only(right: 10),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Ramesh Karki',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).accentColor),
                  ),
                  Text(
                    'Verified user',
                    style: TextStyle(fontSize: 10, color: Colors.grey),
                  )
                ],
              ),
              Container(child: Icon(Icons.delete,color: Colors.red,),padding: EdgeInsets.only(left:100),),
            ],
          ),
        ),
         Card(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                child: CircleAvatar(
                  backgroundImage: AssetImage(
                      'assets/images/male.png'),
                ),
                padding: EdgeInsets.only(right: 10),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Ramesh Karki',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).accentColor),
                  ),
                  Text(
                    'Verified user',
                    style: TextStyle(fontSize: 10, color: Colors.grey),
                  )
                ],
              ),
              Container(child: Icon(Icons.delete,color: Colors.red,),margin: EdgeInsets.only(left:100),),
            ],
          ),
        )
      ],
    );
  }
}
