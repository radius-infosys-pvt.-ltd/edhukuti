import 'package:edhukuti/constants/color_constants.dart';
import 'package:edhukuti/models/message_model.dart';
import 'package:edhukuti/screens/ChatScreen.dart';
import 'package:edhukuti/widgets/active_pool_members.dart';
import 'package:edhukuti/widgets/circle_progress_bar.dart';
import 'package:edhukuti/widgets/my_flutter_app_icons.dart';
import 'package:flutter/material.dart';

class SingleMoneyPools extends StatefulWidget {
  SingleMoneyPools({Key key}) : super(key: key);

  @override
  _SingleMoneyPoolsState createState() => _SingleMoneyPoolsState();
}

class _SingleMoneyPoolsState extends State<SingleMoneyPools> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Maharjan Money Pool'),
        elevation: 0.0,
      ),
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(children: <Widget>[
            Container(
              height: 250.0,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(20),
                      bottomRight: Radius.circular(20)),
                  color: primaryGreen),
            ),
            Center(
              child: Column(
                children: [
                  SizedBox(height: 10),
                  CircularPercentIndicator(
                    radius: 150.0,
                    lineWidth: 10.0,
                    percent: 0.8,
                    center: Text(
                      '80%',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Philosopher',
                          fontSize: 35),
                    ),
                    progressColor: Colors.green,
                  ),
                  SizedBox(height: 5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Pool Amount:',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Philosopher',
                            fontSize: 20),
                      ),
                      Text(
                        ' \$80,000',
                        style: TextStyle(
                            color: Colors.white, fontFamily: 'Philosopher'),
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Active Memebers:',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Philosopher',
                            fontSize: 20),
                      ),
                      Text(
                        ' 5',
                        style: TextStyle(
                            color: Colors.white, fontFamily: 'Philosopher'),
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                ],
              ),
            ),
          ]),
          SizedBox(height: 20),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            height: 220,
            child: ListView(
              scrollDirection: Axis.vertical,
              children: [
                Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          'Active Members',
                          style: TextStyle(
                              color: Theme.of(context).secondaryHeaderColor,
                              fontSize: 20,
                              fontFamily: 'Philosopher',
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                    SizedBox(height: 10),
                    ActivePoolMembers(),
                    ActivePoolMembers(),
                    ActivePoolMembers(),
                    ActivePoolMembers()
                  ],
                ),
              ],
            ),
          ),
        ],
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ChatScreen(user: chats[2].sender)));
        },
        child: Icon(
          MyFlutterApp.chat,
          color: Colors.white,
        ),
        backgroundColor: primaryGreen,
      ),
    );
  }
}
