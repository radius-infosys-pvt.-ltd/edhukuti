import 'package:edhukuti/screens/add_contribution.dart';
import 'package:edhukuti/widgets/budgets.dart';
import 'package:edhukuti/widgets/recent_fund_transacton.dart';
import 'package:edhukuti/widgets/recurring_contribution.dart';
import 'package:flutter/material.dart';
import '../constants/color_constants.dart';

class Contribution extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contributions'),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Stack(children: <Widget>[
            Container(
              height: 100.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20)),
                color: primaryGreen,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Total Contributions',
                      style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontFamily: 'Philosopher',
                          fontSize: 16),
                    ),
                    Text(
                      '\$ 20,00,000',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 30.0,
                          fontFamily: 'Philosopher'),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 80),
              padding: EdgeInsets.symmetric(horizontal: 20),
              height: 90,
              child: Card(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(top: 8.0, bottom: 6),
                          padding: const EdgeInsets.symmetric(
                              vertical: 3.0, horizontal: 8),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.blue),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Row(
                            children: [
                              Text(
                                'Contribution',
                                style: TextStyle(fontSize: 10),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.arrow_upward,
                              size: 18,
                              color: Colors.green,
                            ),
                            Text(' \$5,00,000'),
                          ],
                        )
                      ],
                    ),
                    VerticalDivider(
                      indent: 10,
                      endIndent: 10,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(top: 8.0, bottom: 6),
                          padding: const EdgeInsets.symmetric(
                              vertical: 3.0, horizontal: 8),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.red),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Row(
                            children: [
                              Text(
                                'Expenditure',
                                style: TextStyle(fontSize: 10),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.arrow_downward,
                              size: 18,
                              color: Colors.red,
                            ),
                            Text(' \$5,00,000'),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
            )
          ]),
          Container(
            padding: EdgeInsets.all(12.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Recurring Contributions',
                      style: TextStyle(
                          color: Theme.of(context).secondaryHeaderColor,
                          fontSize: 20.0,
                          fontFamily: 'Philosopher',
                          fontWeight: FontWeight.bold),
                    ),
                    FlatButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddContribution()));
                        },
                        child: Row(
                          children: [
                            Icon(
                              Icons.add,
                              size: 15,
                              color: Theme.of(context).secondaryHeaderColor,
                            ),
                            Text('Add new',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).secondaryHeaderColor,
                                    fontSize: 15,
                                    fontFamily: 'Philosopher'))
                          ],
                        ))
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                RecurringContribution(
                  contribution: 'Travelling',
                  totalContribution: '\$1000',
                  contributionAmount: '\$500',
                  timeFrame: 'Monthly',
                ),
                SizedBox(
                  height: 2,
                ),
                RecurringContribution(
                  contribution: 'Health Care',
                  totalContribution: '\$1000',
                  contributionAmount: '\$500',
                  timeFrame: 'Monthly',
                ),
                SizedBox(
                  height: 2,
                ),
                RecurringContribution(
                  contribution: 'Regular Weekly ',
                  totalContribution: '\$1000',
                  contributionAmount: '\$500',
                  timeFrame: 'Yearly',
                ),
                SizedBox(
                  height: 2,
                ),
                RecurringContribution(
                  contribution: 'Fitness',
                  totalContribution: '\$1000',
                  contributionAmount: '\$500',
                  timeFrame: 'Weekly',
                ),
              ],
            ),
          )
        ]),
      ),
    );
  }
}
