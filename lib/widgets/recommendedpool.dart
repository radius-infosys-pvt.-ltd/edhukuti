import 'package:edhukuti/screens/FindSingleMoneyPool.dart';
import 'package:flutter/material.dart';

class RecommendedPool extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Recommended Pools',
              style: TextStyle(
                  color: Theme.of(context).secondaryHeaderColor,
                  fontFamily: 'Philosopher',
                  fontSize: 22,
                  fontWeight: FontWeight.w600),
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        FlatButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => FindSingleMoneyPool()),
            );
          },
          padding: EdgeInsets.all(0),
          child: Card(
            elevation: 0.4,
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  height: 100,
                  width: 100,
                  decoration: BoxDecoration(
                      color: Theme.of(context).secondaryHeaderColor,
                      border: Border.all(
                        color: Theme.of(context).secondaryHeaderColor,
                        width: 2,
                      ),
                      borderRadius: BorderRadius.circular(50)),
                  child: Center(
                      child: Text(
                    '\$ 80,000',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  )),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Maharjan Money Pool',
                          style: TextStyle(
                              color: Theme.of(context).secondaryHeaderColor,
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              'Pool Amount:',
                              style: TextStyle(
                                  color: Theme.of(context).secondaryHeaderColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Text(
                                '\$ 80,000',
                                style: TextStyle(fontSize: 12),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.people,
                              size: 20,
                              color: Theme.of(context).secondaryHeaderColor,
                            ),
                            Text(' 3', style: TextStyle(fontSize: 12)),
                            SizedBox(
                              width: 20,
                            ),
                            Icon(
                              Icons.calendar_today,
                              color: Theme.of(context).secondaryHeaderColor,
                              size: 20,
                            ),
                            Text(' 12 Months', style: TextStyle(fontSize: 12))
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        Card(
          elevation: 0.4,
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                    color: Theme.of(context).secondaryHeaderColor,
                    border: Border.all(
                      color: Theme.of(context).secondaryHeaderColor,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(50)),
                child: Center(
                    child: Text(
                  '\$ 80,000',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                )),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'RajMahal Money Pool',
                        style: TextStyle(
                            color: Theme.of(context).secondaryHeaderColor,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Text(
                            'Pool Amount:',
                            style: TextStyle(
                                color: Theme.of(context).secondaryHeaderColor,
                                fontSize: 12,
                                fontWeight: FontWeight.w600),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5.0),
                            child: Text(
                              '\$ 80,000',
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.people,
                            size: 20,
                            color: Theme.of(context).secondaryHeaderColor,
                          ),
                          Text(' 3', style: TextStyle(fontSize: 12)),
                          SizedBox(
                            width: 20,
                          ),
                          Icon(
                            Icons.calendar_today,
                            color: Theme.of(context).secondaryHeaderColor,
                            size: 20,
                          ),
                          Text(' 12 Months', style: TextStyle(fontSize: 12))
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        Card(
          elevation: 0.4,
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                    color: Theme.of(context).secondaryHeaderColor,
                    border: Border.all(
                      color: Theme.of(context).secondaryHeaderColor,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(50)),
                child: Center(
                    child: Text(
                  '\$ 80,000',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                )),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Sasto Money Foundation',
                        style: TextStyle(
                            color: Theme.of(context).secondaryHeaderColor,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Text(
                            'Pool Amount:',
                            style: TextStyle(
                                color: Theme.of(context).secondaryHeaderColor,
                                fontSize: 12,
                                fontWeight: FontWeight.w600),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5.0),
                            child: Text(
                              '\$ 80,000',
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.people,
                            size: 20,
                            color: Theme.of(context).secondaryHeaderColor,
                          ),
                          Text(' 3', style: TextStyle(fontSize: 12)),
                          SizedBox(
                            width: 20,
                          ),
                          Icon(
                            Icons.calendar_today,
                            color: Theme.of(context).secondaryHeaderColor,
                            size: 20,
                          ),
                          Text(' 12 Months', style: TextStyle(fontSize: 12))
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        Card(
          elevation: 0.4,
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                    color: Theme.of(context).secondaryHeaderColor,
                    border: Border.all(
                      color: Theme.of(context).secondaryHeaderColor,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(50)),
                child: Center(
                    child: Text(
                  '\$ 80,000',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                )),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Maharjan Money Pool',
                        style: TextStyle(
                            color: Theme.of(context).secondaryHeaderColor,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Text(
                            'Pool Amount:',
                            style: TextStyle(
                                color: Theme.of(context).secondaryHeaderColor,
                                fontSize: 12,
                                fontWeight: FontWeight.w600),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5.0),
                            child: Text(
                              '\$ 80,000',
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.people,
                            size: 20,
                            color: Theme.of(context).secondaryHeaderColor,
                          ),
                          Text(' 3', style: TextStyle(fontSize: 12)),
                          SizedBox(
                            width: 20,
                          ),
                          Icon(
                            Icons.calendar_today,
                            color: Theme.of(context).secondaryHeaderColor,
                            size: 20,
                          ),
                          Text(' 12 Months', style: TextStyle(fontSize: 12))
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
