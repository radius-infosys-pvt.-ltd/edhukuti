import 'package:edhukuti/constants/color_constants.dart';
import 'package:edhukuti/screens/AddBank.dart';
import 'package:edhukuti/widgets/fund_load_date.dart';
import 'package:edhukuti/widgets/fund_load_transaction.dart';
import 'package:flutter/material.dart';
import 'package:intervalprogressbar/intervalprogressbar.dart';

class MoneyPoolDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Load Fund'),
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: [
                Container(
                    height: 100,
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: primaryGreen,
                    ),
                    child: Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 10),
                            Text(
                              'Mahrajan Money Pool',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Philosopher',
                                  fontSize: 18.0),
                            ),
                            Text(
                              'Nipesh Pant',
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontFamily: 'Philosopher',
                                  fontSize: 12.0),
                            ),
                            SizedBox(height: 10),
                            Text(
                              '\$ 80,000',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Philosopher',
                                  fontSize: 22.0),
                            ),
                          ],
                        ),
                      ],
                    )),
                SizedBox(height: 10),
                IntervalProgressBar(
                    direction: IntervalProgressDirection.horizontal,
                    max: 5,
                    progress: 2,
                    intervalSize: 4,
                    size: Size(300, 8),
                    highlightColor: Theme.of(context).primaryColor,
                    defaultColor: Colors.grey,
                    intervalColor: Colors.transparent,
                    intervalHighlightColor: Colors.transparent,
                    reverse: false,
                    radius: 10),
                FundLoadDate(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    FlatButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddBank()));
                        },
                        child: Row(
                          children: [
                            Icon(
                              Icons.add,
                              size: 15,
                              color: Theme.of(context).secondaryHeaderColor,
                            ),
                            Text(
                              ' Load Fund',
                              style: TextStyle(
                                  color:
                                      Theme.of(context).secondaryHeaderColor),
                            )
                          ],
                        )),
                  ],
                ),
                FundLoadTransaction(),
              ],
            ),
          ),
        ));
  }
}
