import 'package:edhukuti/constants/color_constants.dart';
import 'package:edhukuti/models/message_model.dart';
import 'package:edhukuti/screens/ChatScreen.dart';
import 'package:edhukuti/screens/Dashboard.dart';
import 'package:edhukuti/screens/SingleProfile.dart';
import 'package:edhukuti/screens/main/profile.dart';
import 'package:edhukuti/widgets/my_flutter_app_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class ActivePoolMembers extends StatefulWidget {
  @override
  _ActivePoolMembersState createState() => _ActivePoolMembersState();
}

class _ActivePoolMembersState extends State<ActivePoolMembers> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Card(
            elevation: 0.2,
            child: Slidable(
              actionPane: SlidableDrawerActionPane(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                    child: CircleAvatar(
                      backgroundImage: AssetImage('assets/images/male.png'),
                    ),
                    padding: EdgeInsets.only(right: 10),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Ramesh Karki',
                          style: TextStyle(
                              fontSize: 14,
                              color: Theme.of(context).accentColor),
                        ),
                        Text(
                          'Verified user',
                          style: TextStyle(fontSize: 10, color: Colors.grey),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              actions: [
                IconSlideAction(
                  caption: 'View Profile',
                  color: greenColor,
                  icon: Icons.supervised_user_circle,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SingleProfile()));
                  },
                ),
              ],
              secondaryActions: [
                IconSlideAction(
                  color: Colors.blue.shade300,
                  icon: Icons.thumb_up,
                  foregroundColor: Colors.white,
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Dashboard()));
                  },
                ),
                IconSlideAction(
                  color: goldenColor,
                  foregroundColor: Colors.white,
                  icon: Icons.message,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ChatScreen(user: chats[1].sender)));
                  },
                ),
                IconSlideAction(
                  color: greenColor,
                  icon: MyFlutterApp.chat,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ChatScreen(user: chats[2].sender)));
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
