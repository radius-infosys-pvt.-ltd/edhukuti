import 'package:flutter/material.dart';

class RecentFundTransaction extends StatefulWidget {

  @override
  _RecentFundTransactionState createState() => _RecentFundTransactionState();
}

class _RecentFundTransactionState extends State<RecentFundTransaction> {
   List<String> _timeframe = ['Daily', 'Weekly','Monthly','Yearly']; 
   String _selectedTimeFrame;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Details Transactions',style: TextStyle(fontSize: 20,color: Theme.of(context).secondaryHeaderColor,fontFamily: 'Philosopher',fontWeight: FontWeight.bold),),
              DropdownButton(
                underline: SizedBox(),
                hint: Text(_timeframe[0]),
                value: _selectedTimeFrame,
                onChanged: (newValue) {
                  setState(() {
                    _selectedTimeFrame = newValue;
                  });
                },
                items: _timeframe.map((timeFrame) {
                  return DropdownMenuItem(
                    child: new Text(timeFrame),
                    value: timeFrame,
                  );
                }).toList(),
              ),
            ],
          ),
          Card(
            elevation: 0.2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: CircleAvatar(
                    backgroundImage: AssetImage('assets/images/male.png'),
                  ),
                  padding: EdgeInsets.only(right: 10),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'New Contribution',
                        style: TextStyle(
                            fontSize: 14, color: Theme.of(context).accentColor),
                      ),
                      Text(
                        'Nipesh Pant',
                        style: TextStyle(fontSize: 10, color: Colors.grey),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right:8.0),
                  child: Row(
                    children: [
                      Icon(Icons.arrow_upward,size: 20, color: Colors.green,),
                      Text(' \$20,000')
                    ],
                  ),
                )
              ],
            ),
          ),
           Card(
            elevation: 0.2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: CircleAvatar(
                    backgroundImage: AssetImage('assets/images/male.png'),
                  ),
                  padding: EdgeInsets.only(right: 10),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Debt',
                        style: TextStyle(
                            fontSize: 14, color: Theme.of(context).accentColor),
                      ),
                      Text(
                        'Nipesh Pant',
                        style: TextStyle(fontSize: 10, color: Colors.grey),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right:8.0),
                  child: Row(
                    children: [
                      Icon(Icons.arrow_downward,size: 20, color: Colors.red,),
                      Text(' \$20,000')
                    ],
                  ),
                )
              ],
            ),
          ),
           Card(
            elevation: 0.2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: CircleAvatar(
                    backgroundImage: AssetImage('assets/images/male.png'),
                  ),
                  padding: EdgeInsets.only(right: 10),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'New Contribution',
                        style: TextStyle(
                            fontSize: 14, color: Theme.of(context).accentColor),
                      ),
                      Text(
                        'Nipesh Pant',
                        style: TextStyle(fontSize: 10, color: Colors.grey),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right:8.0),
                  child: Row(
                    children: [
                      Icon(Icons.arrow_upward,size: 20, color: Colors.green,),
                      Text(' \$20,000')
                    ],
                  ),
                )
              ],
            ),
          ),
           Card(
            elevation: 0.2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: CircleAvatar(
                    backgroundImage: AssetImage('assets/images/male.png'),
                  ),
                  padding: EdgeInsets.only(right: 10),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'New Contribution',
                        style: TextStyle(
                            fontSize: 14, color: Theme.of(context).accentColor),
                      ),
                      Text(
                        'Nipesh Pant',
                        style: TextStyle(fontSize: 10, color: Colors.grey),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right:8.0),
                  child: Row(
                    children: [
                      Icon(Icons.arrow_upward,size: 20, color: Colors.green,),
                      Text(' \$20,000')
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}