import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class LandingIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/edhukuti_logo.png',
              height: 120,
            ),
          ],
        ),
        Text(
          "e-Dhukuti",
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: 'Philosopher',
              fontSize: 44.0,
              shadows: [
                Shadow(
                  blurRadius: 4,
                  color: Colors.black.withOpacity(0.3),
                  offset: Offset(2.0, 5.0),
                )
              ],
              color: Hexcolor('#B59374')),
        ),
        SizedBox(height: 10),
        Text("The Self Help Bank",
            style: TextStyle(color: Hexcolor('#D2B78E'), fontSize: 20.0)),
      ],
    );
  }
}
