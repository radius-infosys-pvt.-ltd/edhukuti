import 'package:edhukuti/screens/Dashboard.dart';
import '../screens/Dashboard.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class Rules extends StatefulWidget {
  @override
  _RulesState createState() => _RulesState();
}

class _RulesState extends State<Rules> {
  bool isfees = false;
  bool isverified = false;
  bool isGurantor = false;
  bool isInvite = false;
  bool isOngoing = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                'Rules and Regulation',
                style: TextStyle(
                    color: Theme.of(context).secondaryHeaderColor,
                    fontSize: 22,
                    fontFamily: 'Philosopher',
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(child: Text('Do you want to add late payment fees?')),
              Switch(
                value: isfees,
                onChanged: (value) {
                  setState(() {
                    isfees = value;
                  });
                },
                activeTrackColor: Colors.grey,
                activeColor: Theme.of(context).accentColor,
              ),
            ],
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  child: Text('Do users needs to be completely verified?')),
              Switch(
                value: isverified,
                onChanged: (value) {
                  setState(() {
                    isverified = value;
                  });
                },
                activeTrackColor: Colors.grey,
                activeColor: Theme.of(context).accentColor,
              ),
            ],
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  child: Text(
                      'Do user need Gurantor before scoring the pot amount?')),
              Switch(
                value: isGurantor,
                onChanged: (value) {
                  setState(() {
                    isGurantor = value;
                  });
                },
                activeTrackColor: Colors.grey,
                activeColor: Theme.of(context).accentColor,
              ),
            ],
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  child: Text('Do user can invite thier friends and family?')),
              Switch(
                value: isInvite,
                onChanged: (value) {
                  setState(() {
                    isInvite = value;
                  });
                },
                activeTrackColor: Colors.grey,
                activeColor: Theme.of(context).accentColor,
              ),
            ],
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(child: Text('Is it going to be one-off or ongoing?')),
              Switch(
                value: isOngoing,
                onChanged: (value) {
                  setState(() {
                    isOngoing = value;
                  });
                },
                activeTrackColor: Colors.grey,
                activeColor: Theme.of(context).accentColor,
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 30, bottom: 10),
            height: 50,
            width: 325,
            child: RaisedButton(
              shape: StadiumBorder(),
              color: Hexcolor('#DAAD68'),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Dashboard()));
              },
              child: Text(
                "Create a Pool",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.w600),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
