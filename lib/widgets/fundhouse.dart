import 'package:edhukuti/constants/color_constants.dart';
import 'package:edhukuti/widgets/recent_fund_transacton.dart';
import 'package:flutter/material.dart';

class FundHouse extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Fund House'),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Stack(children: <Widget>[
            Container(
              height: 100.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20)),
                color: primaryGreen,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Available Balance',
                      style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontFamily: 'Philosopher',
                          fontSize: 16),
                    ),
                    Text(
                      '\$ 20,00,000',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 30.0,
                          fontFamily: 'Philosopher'),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 80),
              padding: EdgeInsets.symmetric(horizontal: 20),
              height: 90,
              child: Card(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(top: 8.0, bottom: 6),
                          padding: const EdgeInsets.symmetric(
                              vertical: 3.0, horizontal: 8),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.blue),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Row(
                            children: [
                              Text(
                                'Contribution',
                                style: TextStyle(fontSize: 10),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.arrow_upward,
                              size: 18,
                              color: Colors.green,
                            ),
                            Text(' \$5,00,000'),
                          ],
                        )
                      ],
                    ),
                    VerticalDivider(
                      indent: 10,
                      endIndent: 10,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(top: 8.0, bottom: 6),
                          padding: const EdgeInsets.symmetric(
                              vertical: 3.0, horizontal: 8),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.blue),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Row(
                            children: [
                              Text(
                                'Debt',
                                style: TextStyle(fontSize: 10),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.arrow_downward,
                              size: 18,
                              color: Colors.red,
                            ),
                            Text(' \$5,00,000'),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
            )
          ]),
          RecentFundTransaction(),
        ]),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => FundHouse()));
        },
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        backgroundColor: primaryGreen,
      ),
    );
  }
}
