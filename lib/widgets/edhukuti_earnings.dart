import 'package:edhukuti/constants/color_constants.dart';
import 'package:edhukuti/widgets/invitedmember.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class EdhukutiEarnings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edhukuti Earnings'),
        elevation: 0,
      ),
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(children: <Widget>[
            Container(
              height: 230.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20)),
                color: primaryGreen,
              ),
            ),
            Center(
              child: Column(
                children: [
                  SizedBox(height: 10),
                  Container(
                      height: 150,
                      child: Image.asset('assets/images/e-dhukuti.png')),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Total Earnings:',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Philosopher',
                            fontSize: 20),
                      ),
                      Text(
                        ' 80,00 Dhu',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Philosopher',
                            fontSize: 18.0),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ]),
          SizedBox(height: 10),
          Container(
            height: 270,
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: ListView(
              scrollDirection: Axis.vertical,
              children: [
                Column(
                  children: [
                    InvitedMembers(),
                    Container(
                      margin: EdgeInsets.only(top: 20, bottom: 20),
                      height: 50,
                      width: 325,
                      child: RaisedButton(
                        shape: StadiumBorder(),
                        color: Hexcolor('#DAAD68'),
                        onPressed: () {},
                        child: Text(
                          "Refer a Friend",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      )),
    );
  }
}
