import 'package:edhukuti/screens/addmoneypool.dart';
import 'package:edhukuti/screens/findmoneypool.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hexcolor/hexcolor.dart';
import '../widgets/my_flutter_app_icons.dart';
import 'package:flutter/material.dart';
import './bottom_navigation.dart';
import '../constants/color_constants.dart';
import './topbar.dart';
import 'dart:ui';
import "./expansion_card.dart";

class BankCard1 extends StatelessWidget {
  const BankCard1({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpansionTileCard(
      contentPadding: EdgeInsets.all(12),
      baseColor: Colors.white,
      borderRadius: BorderRadius.circular(0),
      expandedTextColor: goldenColor,
      elevation: 3,
      title: Container(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'SAVING ACCOUNT',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w800,
            ),
          ),
          Text(
            'SAPHAL GHIMIRE',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w400,
            ),
          ),
          Text(
            '10200349382910',
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w200,
            ),
          ),
        ],
      )),
      children: <Widget>[
        Divider(
          thickness: 1.0,
          height: 1.0,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 8.0,
          ),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Available Balance',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                  Text(
                    '\$20,000',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Actual Balance',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                  Text(
                    '\$21,000',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Accured Interest',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                  Text(
                    '\$6.02',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(
            top: 10,
            bottom: 10,
          ),
          color: Colors.grey[200],
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("My title"),
                            content: Text("This is my message."),
                          );
                        },
                      );
                    },
                    child: Column(
                      children: <Widget>[
                        Icon(FontAwesomeIcons.exchangeAlt,
                            size: 28, color: goldenColor),
                        SizedBox(height: 5),
                        Text(
                          'Send Money',
                          style: TextStyle(
                            fontSize: 12,
                            color: goldenColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("My title"),
                            content: Text("This is my message."),
                          );
                        },
                      );
                    },
                    child: Column(
                      children: <Widget>[
                        Icon(FontAwesomeIcons.wallet,
                            size: 28, color: goldenColor),
                        SizedBox(height: 5),
                        Text(
                          'Payments',
                          style: TextStyle(
                            fontSize: 12,
                            color: goldenColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("My title"),
                            content: Text("This is my message."),
                          );
                        },
                      );
                    },
                    child: Column(
                      children: <Widget>[
                        Icon(MyFlutterApp.statements,
                            size: 28, color: goldenColor),
                        SizedBox(height: 5),
                        Text(
                          'Statements',
                          style: TextStyle(
                            fontSize: 12,
                            color: goldenColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("My title"),
                            content: Text("This is my message."),
                          );
                        },
                      );
                    },
                    child: Column(
                      children: <Widget>[
                        Icon(FontAwesomeIcons.user,
                            size: 28, color: goldenColor),
                        SizedBox(height: 5),
                        Text(
                          'Transfer Money',
                          style: TextStyle(
                            fontSize: 12,
                            color: goldenColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("My title"),
                            content: Text("This is my message."),
                          );
                        },
                      );
                    },
                    child: Column(
                      children: <Widget>[
                        Icon(MyFlutterApp.saving, size: 28, color: goldenColor),
                        SizedBox(height: 5),
                        Text(
                          'Fund House',
                          style: TextStyle(
                            fontSize: 12,
                            color: goldenColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("My title"),
                            content: Text("This is my message."),
                          );
                        },
                      );
                    },
                    child: Column(
                      children: <Widget>[
                        Icon(FontAwesomeIcons.info,
                            size: 28, color: goldenColor),
                        SizedBox(height: 5),
                        Text(
                          'View Details',
                          style: TextStyle(
                            fontSize: 12,
                            color: goldenColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
