import 'package:edhukuti/constants/color_constants.dart';
import 'package:edhukuti/screens/add_budgets.dart';
import 'package:edhukuti/widgets/budgets.dart';
import 'package:edhukuti/widgets/fund_load_date.dart';
import 'package:edhukuti/widgets/fund_load_transaction.dart';
import 'package:edhukuti/widgets/income_expenditure_card.dart';
import 'package:flutter/material.dart';
import 'package:intervalprogressbar/intervalprogressbar.dart';

class Expenditure extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Expenditure'),
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: [
                Container(
                    height: 115,
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: primaryGreen,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/male.png'),
                          ),
                          padding: EdgeInsets.only(right: 10),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Nipesh Pant',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Philosopher',
                                  fontSize: 18.0),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              'Your Spendings',
                              style: TextStyle(
                                  color: Colors.white54,
                                  fontFamily: 'Philosopher',
                                  fontSize: 14.0),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              '\$ 500',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Philosopher',
                                  fontSize: 20.0),
                            ),
                          ],
                        ),
                        SizedBox(height: 5),
                      ],
                    )),
                SizedBox(height: 10),
                IntervalProgressBar(
                    direction: IntervalProgressDirection.horizontal,
                    max: 6,
                    progress: 3,
                    intervalSize: 4,
                    size: Size(300, 8),
                    highlightColor: Theme.of(context).primaryColor,
                    defaultColor: Colors.grey,
                    intervalColor: Colors.transparent,
                    intervalHighlightColor: Colors.transparent,
                    reverse: false,
                    radius: 10),
                SizedBox(
                  height: 5,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '\$1,000',
                        style: TextStyle(
                            color: Theme.of(context).secondaryHeaderColor,
                            fontSize: 12),
                      ),
                      Text(
                        '12 days left',
                        style: TextStyle(
                            color: Theme.of(context).secondaryHeaderColor,
                            fontSize: 12),
                      )
                    ],
                  ),
                ),
                IncomeExpenditureCard(),
                SizedBox(
                  height: 4,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Your Budgets',
                      style: TextStyle(
                          color: Theme.of(context).secondaryHeaderColor,
                          fontSize: 20.0,
                          fontFamily: 'Philosopher',
                          fontWeight: FontWeight.bold),
                    ),
                    FlatButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AddBudget(),
                              ));
                        },
                        child: Row(
                          children: [
                            Icon(Icons.add,
                                size: 12,
                                color: Theme.of(context).secondaryHeaderColor),
                            Text(
                              'Add new budget',
                              style: TextStyle(
                                  color: Theme.of(context).secondaryHeaderColor,
                                  fontSize: 12,
                                  fontFamily: 'Philosopher'),
                            ),
                          ],
                        ))
                  ],
                ),
                Budgets(
                  budget: 'Housing',
                  budgetAmount: '\$500',
                  spentAmount: '\$300',
                  remainingAmount: '\$200',
                ),
                SizedBox(height: 2),
                Budgets(
                  budget: 'Cafe and Coffee',
                  budgetAmount: '\$100',
                  spentAmount: '\$50',
                  remainingAmount: '\$50',
                ),
                SizedBox(height: 2),
                Budgets(
                  budget: 'Entertainment',
                  budgetAmount: '\$200',
                  spentAmount: '\$100',
                  remainingAmount: '\$100',
                ),
              ],
            ),
          ),
        ));
  }
}
