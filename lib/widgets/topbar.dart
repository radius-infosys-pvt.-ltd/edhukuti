import 'package:edhukuti/constants/color_constants.dart';
import "package:flutter/material.dart";
import "../screens/findmoneypool.dart";
import '../widgets/my_flutter_app_icons.dart';

class TopBar extends StatelessWidget {
  const TopBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 8, left: 10),
            child: Row(
              children: <Widget>[
                Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      image: DecorationImage(
                          image: AssetImage('assets/images/user.png'))),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(
                    'Saphal Ghimire',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: <Widget>[
              IconButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => FindMoneyPool()));
                },
                icon: Icon(Icons.search),
                color: Colors.white,
                iconSize: 24,
              ),
              IconButton(
                onPressed: () {},
                icon: Icon(MyFlutterApp.menu),
                color: Colors.white,
                iconSize: 24,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
