import 'package:edhukuti/screens/edit_contribution.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';
import '../constants/color_constants.dart';
import 'dart:ui';
import "./expansion_card.dart";

class RecurringContribution extends StatelessWidget {

  final String contribution;
  final String totalContribution;
  final String contributionAmount;
  final String timeFrame;
  RecurringContribution({this.contribution,this.totalContribution,this.contributionAmount,this.timeFrame,});

  @override
  Widget build(BuildContext context) {
    return ExpansionTileCard(
      contentPadding: EdgeInsets.all(12),
      baseColor: Colors.white,
      borderRadius: BorderRadius.circular(0),
      expandedTextColor: goldenColor,
      elevation: 3,
      title: Container(
          child: Row(
        children: [
          Container(
            height: 30,
            child: Image.asset('assets/images/expense.png'),
          ),
          SizedBox(
            width: 20,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '$contribution',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w800,
                ),
              ),
              Text(
                '$totalContribution',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w200,
                ),
              ),
            ],
          ),
        ],
      )),
      children: <Widget>[
        Divider(
          thickness: 1.0,
          height: 1.0,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 8.0,
          ),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Total Contribution',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                  Text(
                    '$totalContribution',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Contribution Amount',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                  Text(
                    '$contributionAmount',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Time Frame',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                  Text(
                    '$timeFrame',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(
            top: 15,
            bottom: 10,
          ),
          color: Colors.grey[200],
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                 
                  GestureDetector(
                    onTap: () {
                       Navigator.push(context, MaterialPageRoute(builder: (context)=>EditContribution()));
                        },
                      
                   
                    child: Column(
                      children: <Widget>[
                        Icon(FontAwesomeIcons.wallet,
                            size: 20, color: goldenColor),
                        SizedBox(height: 5),
                        Text(
                          'Edit',
                          style: TextStyle(
                            fontSize: 12,
                            color: goldenColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("My title"),
                            content: Text("This is my message."),
                          );
                        },
                      );
                    },
                    child: Column(
                      children: <Widget>[
                        Icon(FontAwesomeIcons.info,
                            size: 20, color: goldenColor),
                        SizedBox(height: 5),
                        Text(
                          'View Details',
                          style: TextStyle(
                            fontSize: 12,
                            color: goldenColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                   GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("My title"),
                            content: Text("This is my message."),
                          );
                        },
                      );
                    },
                    child: Column(
                      children: <Widget>[
                        Icon(Icons.delete,
                            size: 20, color: goldenColor),
                        SizedBox(height: 5),
                        Text(
                          'Delete',
                          style: TextStyle(
                            fontSize: 12,
                            color: goldenColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
             
            ],
          ),
        ),
      ],
    );
  }
}
