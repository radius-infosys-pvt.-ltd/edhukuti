import 'package:edhukuti/screens/addmoneypool.dart';
import 'package:edhukuti/screens/findmoneypool.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import './ActiveMoneyPools.dart';
import '../widgets/my_flutter_app_icons.dart';
import 'package:flutter/material.dart';
import '../widgets/bottom_navigation.dart';
import '../constants/color_constants.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  List<String> _transactionList = ['All', 'Recent'];
  String _selectedTxn;

  @override
  void initState() {
    super.initState();
    _selectedTxn = _transactionList[0];
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: GestureDetector(
          child: Scaffold(
            backgroundColor: Color.fromRGBO(3, 101, 98, 0.8),
            body: SafeArea(
              child: Container(
                child: CustomPaint(
                  painter: ShapesPainter(),
                  child: ListView(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top: 20, left: 10),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/user.png'))),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 12.0),
                                  child: Text(
                                    'Saphal Ghimire',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: Row(
                              children: <Widget>[
                                IconButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                FindMoneyPool()));
                                  },
                                  icon: Icon(Icons.search),
                                  color: Colors.white,
                                  iconSize: 24,
                                ),
                                IconButton(
                                  onPressed: () {},
                                  icon: Icon(MyFlutterApp.menu),
                                  color: Colors.white,
                                  iconSize: 24,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          padding: const EdgeInsets.only(left: 15.0),
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 255, 255, 0.6),
                              borderRadius: BorderRadius.circular(20)),
                          height: 120,
                          width: double.infinity,
                          child: Expanded(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(top: 25),
                                        child: Text(
                                          'Balance',
                                          style: TextStyle(
                                              color: Colors.black54,
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      Text(
                                        '\$ 40,00.98',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 32,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                                RawMaterialButton(
                                  onPressed: () {},
                                  fillColor: Color.fromRGBO(9, 128, 124, 1),
                                  child: Icon(FontAwesomeIcons.bolt,
                                      size: 24.0, color: Colors.white),
                                  padding: EdgeInsets.all(5.0),
                                  shape: CircleBorder(),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                  left: 20,
                                  bottom: 10,
                                ),
                                child: Text(
                                  'Operations',
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: darkGoldenColor,
                                      fontFamily: 'Philosopher',
                                      fontWeight: FontWeight.w700),
                                ),
                              ),
                            ),
                            Container(
                              height: 120.0,
                              child: new ListView(
                                scrollDirection: Axis.horizontal,
                                children: <Widget>[
                                  FlatButton(
                                    padding: EdgeInsets.only(left: 8),
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                AddMoneyPools()),
                                      );
                                    },
                                    child: Container(
                                      margin:
                                          EdgeInsets.only(top: 5, bottom: 5),
                                      width: 120.0,
                                      decoration: BoxDecoration(
                                          color: Color.fromRGBO(
                                              255, 255, 255, 0.8),
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            MyFlutterApp.pools,
                                            size: 40,
                                            color: greenColor,
                                          ),
                                          SizedBox(height: 10),
                                          Text(
                                            "Money Pools",
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w700,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  FlatButton(
                                    padding: EdgeInsets.only(left: 14),
                                    onPressed: () {},
                                    child: Container(
                                      margin:
                                          EdgeInsets.only(top: 5, bottom: 5),
                                      width: 120.0,
                                      decoration: BoxDecoration(
                                          color: Color.fromRGBO(
                                              255, 255, 255, 0.8),
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            MyFlutterApp.banks,
                                            size: 40,
                                            color: greenColor,
                                          ),
                                          SizedBox(height: 10),
                                          Text(
                                            "Connect to Bank",
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w700,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  FlatButton(
                                    padding: EdgeInsets.only(left: 14),
                                    onPressed: () {},
                                    child: Container(
                                      margin: EdgeInsets.only(
                                          top: 5, bottom: 5, right: 5),
                                      width: 120.0,
                                      decoration: BoxDecoration(
                                          color: Color.fromRGBO(
                                              255, 255, 255, 0.8),
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            MyFlutterApp.statements,
                                            size: 40,
                                            color: greenColor,
                                          ),
                                          SizedBox(height: 10),
                                          Text(
                                            "Get Statements",
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w700,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          height: 260,
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 20, top: 20),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Transaction History',
                                      style: TextStyle(
                                          fontSize: 20,
                                          color: darkGoldenColor,
                                          fontFamily: 'Philosopher',
                                          fontWeight: FontWeight.w700),
                                    ),
                                    Container(
                                        height: 20,
                                        padding: EdgeInsets.only(top: 3),
                                        // decoration: BoxDecoration(
                                        //     border:
                                        //         Border.all(color: goldenColor),
                                        //     borderRadius:
                                        //         BorderRadius.circular(30)),
                                        child: FlatButton(
                                            onPressed: () {
                                              /*...*/
                                            },
                                            textColor: goldenColor,
                                            child: Column(
                                              children: <Widget>[
                                                Text(
                                                  'View All',
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      color: darkGoldenColor,
                                                      fontWeight:
                                                          FontWeight.w700),
                                                ),
                                              ],
                                            ))
                                        // child: Padding(
                                        //   padding:
                                        //       const EdgeInsets.only(left: 8.0),
                                        //   child: PopupMenuButton<String>(
                                        //     itemBuilder: (context) {
                                        //       return _transactionList.map((str) {
                                        //         return PopupMenuItem(
                                        //           value: str,
                                        //           child: Text(
                                        //             str,
                                        //             style: TextStyle(
                                        //               color: goldenColor,
                                        //             ),
                                        //           ),
                                        //         );
                                        //       }).toList();
                                        //     },
                                        //     child: Row(
                                        //       mainAxisSize: MainAxisSize.min,
                                        //       children: <Widget>[
                                        //         Text(
                                        //           _selectedTxn,
                                        //           style: TextStyle(
                                        //               color: goldenColor),
                                        //         ),
                                        //         Icon(
                                        //           Icons.arrow_drop_down,
                                        //           color: goldenColor,
                                        //         ),
                                        //       ],
                                        //     ),
                                        //     onSelected: (v) {
                                        //       setState(() {
                                        //         _selectedTxn = v;
                                        //       });
                                        //     },
                                        //   ),
                                        // ),
                                        )
                                  ],
                                ),
                              ),
                              Container(
                                height: 60,
                                margin: EdgeInsets.only(
                                    top: 5, left: 15, right: 15),
                                width: double.infinity,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.all(10),
                                      height: 40,
                                      width: 40,
                                      decoration: BoxDecoration(
                                        color: greenColor,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Icon(
                                        FontAwesomeIcons.briefcase,
                                        color: Colors.white,
                                      ),
                                    ),
                                    Container(
                                      child: Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10.0),
                                              child: Text('Maharaja Pool',
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      color: goldenColor,
                                                      fontWeight:
                                                          FontWeight.w600)),
                                            ),
                                            Text('Apr 20',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey,
                                                    fontWeight:
                                                        FontWeight.w600)),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 0),
                                      child: Text('+\$2,600',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black,
                                              fontWeight: FontWeight.w700)),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: 60,
                                margin: EdgeInsets.only(
                                    top: 5, left: 15, right: 15),
                                width: double.infinity,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.all(10),
                                      height: 40,
                                      width: 40,
                                      decoration: BoxDecoration(
                                        color: greenColor,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Icon(
                                        FontAwesomeIcons.briefcase,
                                        color: Colors.white,
                                      ),
                                    ),
                                    Container(
                                      child: Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10.0),
                                              child: Text('Rajashree Pool',
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      color: goldenColor,
                                                      fontWeight:
                                                          FontWeight.w600)),
                                            ),
                                            Text('Apr 20',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey,
                                                    fontWeight:
                                                        FontWeight.w600)),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 0),
                                      child: Text('-\$600',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black,
                                              fontWeight: FontWeight.w700)),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: 60,
                                margin: EdgeInsets.only(
                                    top: 5, left: 15, right: 15),
                                width: double.infinity,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.all(10),
                                      height: 40,
                                      width: 40,
                                      decoration: BoxDecoration(
                                        color: greenColor,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Icon(
                                        FontAwesomeIcons.briefcase,
                                        color: Colors.white,
                                      ),
                                    ),
                                    Container(
                                      child: Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10.0),
                                              child: Text('Maharaja Pool',
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      color: goldenColor,
                                                      fontWeight:
                                                          FontWeight.w600)),
                                            ),
                                            Text('Sept 20',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.grey,
                                                    fontWeight:
                                                        FontWeight.w600)),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 0),
                                      child: Text('+\$2,200',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black,
                                              fontWeight: FontWeight.w700)),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            bottomNavigationBar: FABBottomAppBar(
              color: Colors.grey,
              backgroundColor: Colors.white,
              selectedColor: Theme.of(context).primaryColor,
              notchedShape: CircularNotchedRectangle(),
              items: [
                FABBottomAppBarItem(iconData: MyFlutterApp.home, text: 'Home'),
                FABBottomAppBarItem(
                    iconData: MyFlutterApp.wallet, text: 'Money'),
                FABBottomAppBarItem(
                    iconData: MyFlutterApp.saving, text: 'Fund'),
                FABBottomAppBarItem(
                    iconData: MyFlutterApp.profile, text: 'Profile'),
              ],
            ),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerDocked,
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddMoneyPool()));
              },
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
              backgroundColor: goldenColor,
              elevation: 2.0,
            ),
          ),
        ),
      ),
    );
  }
}

class ShapesPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    // paint.color = Colors.red;
    // var rect = Rect.fromLTWH(0, 0, size.width, size.height);
    // canvas.drawRect(rect, paint);
    paint.color = Color.fromRGBO(9, 128, 124, 0.5);
    // var sideCircle = Offset(50, size.height * 0.45);
    // canvas.drawCircle(sideCircle, size.width * 0.5, paint);
    // var topCircle = Offset(size.width, 0);
    // canvas.drawCircle(topCircle, 70, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
