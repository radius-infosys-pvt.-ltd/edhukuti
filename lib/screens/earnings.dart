import 'package:edhukuti/widgets/contribution.dart';
import 'package:edhukuti/widgets/edhukuti_earnings.dart';
import 'package:edhukuti/widgets/expenditure.dart';
import 'package:edhukuti/widgets/fundhouse.dart';
import 'package:edhukuti/widgets/moneypool_details.dart';
import 'package:edhukuti/widgets/single_moneypool.dart';
import 'package:flutter/material.dart';

class Earnings extends StatefulWidget {
  @override
  _MoneyState createState() => _MoneyState();
}

class _MoneyState extends State<Earnings> {
  
   PageController _controller = PageController(
    initialPage: 1,
  );

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _controller,
      children: [
        Expenditure(),
        EdhukutiEarnings(),
        Contribution()
      ],
    );
  }
}