import 'package:edhukuti/screens/findmoneypool.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../ActiveMoneyPools.dart';
import '../../widgets/my_flutter_app_icons.dart';
import 'package:flutter/material.dart';
import '../../constants/color_constants.dart';
import 'dart:core';
import 'dart:ui';
import "../../widgets/notification_swiper.dart";

class NotificationPage extends StatefulWidget {
  NotificationPage({Key key}) : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SafeArea(
        child: Container(
          child: ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 8, left: 10),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                              'Notifications',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        IconButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FindMoneyPool()));
                          },
                          icon: Icon(Icons.notifications),
                          color: Colors.white,
                          iconSize: 24,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 8,
              ),
              NotificationSwiper(),
            ],
          ),
        ),
      ),
    );
  }
}
