import 'package:edhukuti/screens/findmoneypool.dart';
import '../../widgets/my_flutter_app_icons.dart';
import 'package:flutter/material.dart';
import '../../constants/color_constants.dart';
import 'dart:ui';
import '../AddBank.dart';
import "../../widgets/expansion_card.dart";
import "../../widgets/bank_card.dart";
import "../../widgets/bank_card1.dart";

class Bank extends StatefulWidget {
  @override
  _BankState createState() => _BankState();
}

class _BankState extends State<Bank> {
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Container(
      child: SafeArea(
        child: Container(
          child: ListView(
            children: <Widget>[
              Container(
                width: double.infinity,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 16, left: 14),
                      child: Text(
                        'Accounts',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    IconButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => AddBank()));
                      },
                      icon: Icon(Icons.add),
                      color: Colors.white,
                      iconSize: 24,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                padding: EdgeInsets.all(4),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    BankCard(),
                    SizedBox(height: 15),
                    BankCard1(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
