import 'package:animated_theme_switcher/animated_theme_switcher.dart';
import 'package:edhukuti/constants/color_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import './constants.dart';
import './profile_list_item.dart';
import 'package:flutter/material.dart';
import 'dart:core';
import 'dart:ui';
import '../SingleProfile.dart';
import "package:step_progress_indicator/step_progress_indicator.dart";
import '../EditProfile.dart';
import '../RecentActivities.dart';

class Profile extends StatefulWidget {
  Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, height: 896, width: 414, allowFontScaling: true);

    var profileInfo = Expanded(
      child: Card(
        margin: EdgeInsets.only(top: kSpacingUnit.w * 2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              height: kSpacingUnit.w * 10,
              width: kSpacingUnit.w * 10,
              margin: EdgeInsets.only(
                  bottom: kSpacingUnit.w * 1.5,
                  top: kSpacingUnit.w * 1.5,
                  left: kSpacingUnit.w * 2),
              child: Stack(
                children: <Widget>[
                  CircleAvatar(
                    radius: kSpacingUnit.w * 5,
                    backgroundImage: AssetImage('assets/images/profile.jpg'),
                  ),
                ],
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Saphal Ghimire',
                  style: kTitleTextStyle,
                ),
                SizedBox(height: kSpacingUnit.w * 0.5),
                Text(
                  'gsaphal99@gmail.com',
                  style: kCaptionTextStyle,
                ),
                SizedBox(height: kSpacingUnit.w * 1),
                StepProgressIndicator(
                  totalSteps: 5,
                  currentStep: 2,
                  selectedColor: greenColor,
                  unselectedColor: Colors.grey,
                ),
                SizedBox(height: kSpacingUnit.w * 0.8),
                Text(
                  '40%  Completed',
                  style: kCaptionTextStyle,
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Icon(
                  Icons.warning,
                  color: Colors.red[700],
                ),
                SizedBox(height: kSpacingUnit.w * 1),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(kSpacingUnit.w * 3),
                    color: goldenColor,
                  ),
                  child: Text(
                    'Verify Now',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 12,
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );

    var header = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        profileInfo,
      ],
    );

    return Column(
      children: <Widget>[
        SizedBox(height: kSpacingUnit.w * 1),
        Container(
          margin: EdgeInsets.only(top: 8, left: 10),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                  'Profile',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ),
        ),
        header,
        SizedBox(height: kSpacingUnit.w * 3),
        Expanded(
          child: ListView(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SingleProfile()),
                  );
                },
                child: ProfileListItem(
                  hasNavigation: true,
                  icon: LineAwesomeIcons.question_circle,
                  text: 'View Profile',
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => EditProfilePage()),
                  );
                },
                child: ProfileListItem(
                  hasNavigation: true,
                  icon: LineAwesomeIcons.user_plus,
                  text: 'Edit Profile',
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => RecentActivities()),
                  );
                },
                child: ProfileListItem(
                  icon: LineAwesomeIcons.user_plus,
                  text: 'Recent Activities',
                ),
              ),
              ProfileListItem(
                icon: LineAwesomeIcons.cog,
                text: 'Settings',
              ),
              ProfileListItem(
                icon: LineAwesomeIcons.alternate_sign_out,
                text: 'Logout',
                hasNavigation: false,
              ),
            ],
          ),
        )
      ],
    );
  }
}
