import 'package:edhukuti/screens/AddBank.dart';
import 'package:edhukuti/screens/ChatInbox.dart';
import 'package:edhukuti/screens/RecentActivities.dart';
import 'package:edhukuti/screens/Transactions.dart';
import 'package:edhukuti/screens/addmoneypool.dart';
import 'package:edhukuti/screens/main/nav_drawer.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../widgets/my_flutter_app_icons.dart';
import 'package:flutter/material.dart';
import '../../widgets/bottom_navigation.dart';
import '../../constants/color_constants.dart';
import '../earnings.dart';
import './dashboard.dart';

import './bank.dart';
import '../../widgets/pop_up.dart';
import './notification.dart';
import './profile.dart';
import './nav_drawer.dart';

class HomeLayout extends StatefulWidget {
  @override
  _HomeLayoutState createState() => _HomeLayoutState();
}

class _HomeLayoutState extends State<HomeLayout> {
  PopupMenu menu;
  GlobalKey btnKey = GlobalKey();
  GlobalKey btnKey2 = GlobalKey();
  GlobalKey btnKey3 = GlobalKey();
  int _selectedIndex = 0;
  static final List<Widget> _widgetOptions = <Widget>[
    Dashboard(),
    Bank(),
    NotificationPage(),
    Profile()
  ];

  List<String> _transactionList = ['All', 'Recent'];
  String _selectedTxn;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    _selectedTxn = _transactionList[0];
  }

  void stateChanged(bool isShow) {
    print('menu is ${isShow ? 'showing' : 'closed'}');
  }

  void onClickMenu(MenuItemProvider item) {
    if (item.menuTitle == "Fund House") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Earnings()),
      );
    }
    if (item.menuTitle == "Add a Bank") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => AddBank()),
      );
    }
    if (item.menuTitle == "Money Pools") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => AddMoneyPool()),
      );
    }
    if (item.menuTitle == "View Statements") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Transactions()),
      );
    }
    if (item.menuTitle == "Mentions") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => RecentActivities()),
      );
    }
    if (item.menuTitle == "Chat") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ChatInbox()),
      );
    }
    print('Click menu -> ${item.menuTitle}');
  }

  void onDismiss() {
    print('Menu is dismiss');
  }

  void maxColumn() {
    PopupMenu menu = PopupMenu(
        backgroundColor: Color.fromRGBO(255, 255, 255, 0.4),
        lineColor: Color.fromRGBO(255, 255, 255, 0.4),
        maxColumn: 3,
        items: [
          MenuItem(
              title: 'Add a Bank',
              image: Icon(
                MyFlutterApp.banks,
                size: 32,
                color: Colors.black,
              )),
          MenuItem(
              title: 'Money Pools',
              image: Icon(
                MyFlutterApp.money_bag,
                size: 32,
                color: Colors.black,
              )),
          MenuItem(
              title: 'Fund House',
              image: Icon(
                MyFlutterApp.saving,
                size: 32,
                color: Colors.black,
              )),
          MenuItem(
              title: 'View Statements',
              image: Icon(
                MyFlutterApp.statements,
                size: 32,
                color: Colors.black,
              )),
          MenuItem(
              title: 'Mentions',
              image: Icon(
                FontAwesomeIcons.user,
                size: 32,
                color: Colors.black,
              )),
          MenuItem(
              title: 'Chat',
              image: Icon(
                MyFlutterApp.chat,
                size: 32,
                color: Colors.black,
              ))
        ],
        onClickMenu: onClickMenu,
        stateChanged: stateChanged,
        onDismiss: onDismiss);
    menu.show(widgetKey: btnKey);
  }

  @override
  Widget build(BuildContext context) {
    PopupMenu.context = context;

    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Scaffold(
          backgroundColor: Color.fromRGBO(3, 101, 98, 0.8),
          body: Container(
            child: _widgetOptions.elementAt(_selectedIndex),
          ),
          endDrawer: NavDrawer(),
          bottomNavigationBar: FABBottomAppBar(
            color: Colors.grey,
            backgroundColor: Colors.white,
            selectedColor: Theme.of(context).primaryColor,
            onTabSelected: _onItemTapped,
            notchedShape: CircularNotchedRectangle(),
            items: [
              FABBottomAppBarItem(iconData: MyFlutterApp.home, text: 'Home'),
              FABBottomAppBarItem(
                  iconData: MyFlutterApp.wallet_1, text: 'Money'),
              FABBottomAppBarItem(iconData: MyFlutterApp.bell, text: 'Fund'),
              FABBottomAppBarItem(
                  iconData: MyFlutterApp.profile, text: 'Profile'),
            ],
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          floatingActionButton: FloatingActionButton(
            key: btnKey,
            onPressed: maxColumn,
            child: Icon(
              MyFlutterApp.khutruke_1,
              size: 32,
              color: Colors.white,
            ),
            backgroundColor: greenColor,
            elevation: 2.0,
          ),
        ),
      ),
    );
  }
}

class ShapesPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    // paint.color = Colors.red;
    // var rect = Rect.fromLTWH(0, 0, size.width, size.height);
    // canvas.drawRect(rect, paint);
    paint.color = Color.fromRGBO(9, 128, 124, 0.5);
    // var sideCircle = Offset(50, size.height * 0.45);
    // canvas.drawCircle(sideCircle, size.width * 0.5, paint);
    // var topCircle = Offset(size.width, 0);
    // canvas.drawCircle(topCircle, 70, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
