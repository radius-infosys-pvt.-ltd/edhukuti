import 'package:edhukuti/screens/AddBank.dart';
import 'package:edhukuti/screens/findmoneypool.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../ActiveMoneyPools.dart';
import '../../widgets/my_flutter_app_icons.dart';
import 'package:flutter/material.dart';
import '../../constants/color_constants.dart';
import 'dart:core';
import '../../widgets/topbar.dart';
import 'dart:ui';
import '../TotalBalance.dart';
import '../Transactions.dart';

class Dashboard extends StatefulWidget {
  Dashboard({Key key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SafeArea(
        child: Container(
          child: ListView(
            children: <Widget>[
              TopBar(),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                    child: Container(
                      padding: const EdgeInsets.only(left: 20.0),
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(255, 255, 255, 0.6),
                          borderRadius: BorderRadius.circular(20)),
                      height: 120,
                      width: double.infinity,
                      child: Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(top: 30),
                                    child: Text(
                                      '\$ 40,00.98',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 28,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      'Current Balance',
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            RawMaterialButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => TotalBalance(),
                                  ),
                                );
                              },
                              fillColor: Color.fromRGBO(9, 128, 124, 1),
                              child: Icon(FontAwesomeIcons.plus,
                                  size: 18.0, color: Colors.white),
                              padding: EdgeInsets.all(15.0),
                              shape: CircleBorder(),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.only(
                          left: 10,
                          bottom: 10,
                        ),
                        child: Text(
                          'Operations',
                          style: TextStyle(
                              fontSize: 18,
                              color: darkGoldenColor,
                              fontFamily: 'Philosopher',
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                    ),
                    Container(
                      height: 120.0,
                      child: new ListView(
                        scrollDirection: Axis.horizontal,
                        children: <Widget>[
                          FlatButton(
                            padding: EdgeInsets.only(left: 8),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddMoneyPools()),
                              );
                            },
                            child: Container(
                              margin: EdgeInsets.only(top: 5, bottom: 5),
                              width: 120.0,
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(255, 255, 255, 0.8),
                                  borderRadius: BorderRadius.circular(15)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    MyFlutterApp.pools,
                                    size: 40,
                                    color: greenColor,
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    "Money Pools",
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          FlatButton(
                            padding: EdgeInsets.only(left: 14),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddBank()),
                              );
                            },
                            child: Container(
                              margin: EdgeInsets.only(top: 5, bottom: 5),
                              width: 120.0,
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(255, 255, 255, 0.8),
                                  borderRadius: BorderRadius.circular(15)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    MyFlutterApp.banks,
                                    size: 40,
                                    color: greenColor,
                                  ),
                                  SizedBox(height: 10),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Flexible(
                                        child: Text(
                                          "Add a Bank",
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                          FlatButton(
                            padding: EdgeInsets.only(left: 14),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Transactions()),
                              );
                            },
                            child: Container(
                              margin:
                                  EdgeInsets.only(top: 5, bottom: 5, right: 5),
                              width: 120.0,
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(255, 255, 255, 0.8),
                                  borderRadius: BorderRadius.circular(15)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    MyFlutterApp.statements,
                                    size: 40,
                                    color: greenColor,
                                  ),
                                  SizedBox(height: 10),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Flexible(
                                        child: Text(
                                          "Statements",
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 20, top: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Transactions',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: darkGoldenColor,
                                  fontFamily: 'Philosopher',
                                  fontWeight: FontWeight.w700),
                            ),
                            Container(
                                child: FlatButton(
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                Transactions()),
                                      );
                                    },
                                    textColor: goldenColor,
                                    child: Column(
                                      children: <Widget>[
                                        Text(
                                          'View All',
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: darkGoldenColor,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ],
                                    )))
                          ],
                        ),
                      ),
                      Container(
                        height: 60,
                        margin: EdgeInsets.only(top: 5, left: 10, right: 15),
                        width: double.infinity,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.all(10),
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                color: greenColor,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Icon(
                                FontAwesomeIcons.briefcase,
                                color: Colors.white,
                              ),
                            ),
                            Container(
                              child: Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(top: 10.0),
                                      child: Text('Maharaja Pool',
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: goldenColor,
                                              fontWeight: FontWeight.w600)),
                                    ),
                                    Text('Apr 20',
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w600)),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 0),
                              child: Text('+\$2,600',
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w700)),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 60,
                        margin: EdgeInsets.only(top: 5, left: 10, right: 15),
                        width: double.infinity,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.all(10),
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                color: greenColor,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Icon(
                                FontAwesomeIcons.briefcase,
                                color: Colors.white,
                              ),
                            ),
                            Container(
                              child: Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(top: 10.0),
                                      child: Text('Rajashree Pool',
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: goldenColor,
                                              fontWeight: FontWeight.w600)),
                                    ),
                                    Text('Apr 20',
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w600)),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 0),
                              child: Text('-\$600',
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w700)),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 60,
                        margin: EdgeInsets.only(top: 5, left: 10, right: 15),
                        width: double.infinity,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.all(10),
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                color: greenColor,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Icon(
                                FontAwesomeIcons.briefcase,
                                color: Colors.white,
                              ),
                            ),
                            Container(
                              child: Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(top: 10.0),
                                      child: Text('Maharaja Pool',
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: goldenColor,
                                              fontWeight: FontWeight.w600)),
                                    ),
                                    Text('Sept 20',
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w600)),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 0),
                              child: Text('+\$2,200',
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w700)),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
