import 'package:edhukuti/widgets/searchresult.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class Invite extends StatefulWidget {
  @override
  _InviteState createState() => _InviteState();
}

class _InviteState extends State<Invite> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Invite Friends'),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
            child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 10,
              ),
              Text(
                'Please enter a friend email to invite',
                style: TextStyle(
                    color: Theme.of(context).secondaryHeaderColor,
                    fontSize: 14.0,
                    fontWeight: FontWeight.w600),
              ),
              SizedBox(height: 10,),
                TextField(
                  decoration: InputDecoration(
                    enabledBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                      borderRadius: BorderRadius.circular(10)
                    ),
                    focusedBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                    ),
                    
                    labelText: 'Search',
                    labelStyle: TextStyle(
                        color:Hexcolor('#BDBDBD')),
                    isDense: true,
                    // Added this
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: SearchResult(),
                ),
            ],
          ),
        )),
      ),
    );
  }
}
