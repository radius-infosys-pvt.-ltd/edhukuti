import 'package:edhukuti/widgets/invitedmember.dart';
import 'package:edhukuti/widgets/rules.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class AddMoneyPool extends StatefulWidget {
  @override
  _AddMoneyPoolState createState() => _AddMoneyPoolState();
}

class _AddMoneyPoolState extends State<AddMoneyPool> {
 

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          title: Text('Add Money Pools'),
        ),
        body: SingleChildScrollView(
                  child: Container(
            padding: EdgeInsets.all(12),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 16,
                ),
                TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.attach_money,color: Color.fromARGB(200, 218, 173, 107)),
                    enabledBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                    ),
                    focusedBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                    ),
                    
                    labelText: 'Pool Amount',
                    labelStyle: TextStyle(
                        color:Hexcolor('#ACA9A2')),
                    isDense: true,
                    // Added this
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.calendar_today,color: Color.fromARGB(200, 218, 173, 107)),
                    enabledBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                    ),
                    focusedBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                    ),
                    
                    labelText: 'Pool Duration',
                    labelStyle: TextStyle(
                        color:Hexcolor('#ACA9A2')),
                    isDense: true,
                    // Added this
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.account_balance_wallet,color: Color.fromARGB(200, 218, 173, 107)),
                    enabledBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                    ),
                    focusedBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                    ),
                    
                    labelText: 'Pool Type',
                    labelStyle: TextStyle(
                        color:Hexcolor('#ACA9A2')),
                    isDense: true,
                    // Added this
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.people,color: Color.fromARGB(200, 218, 173, 107)),
                    enabledBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                    ),
                    focusedBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                    ),
                    
                    labelText: 'Number of People',
                    labelStyle: TextStyle(
                        color:Hexcolor('#ACA9A2')),
                    isDense: true,
                    // Added this
                  ),
                ),
                InvitedMembers(),
                Rules(),
              ],
            ),
          ),
        ));
  }
}
