import 'package:edhukuti/screens/congratulation.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:pin_input_text_field/pin_input_text_field.dart';

class OtpVerification extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 100.0, horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Enter the verifiaction code?',
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontWeight: FontWeight.w600)),
                Text('Type the verification code that was sent you by sms',
                    style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.white,
                        fontWeight: FontWeight.normal)),
                PinInputTextField(
                  pinLength: 4,
                  decoration: UnderlineDecoration(
                      colorBuilder:
                          PinListenColorBuilder(Colors.grey, Colors.white)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 200.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                          if (_formKey.currentState.validate()) {}
                        },
                        child: Row(
                          children: [
                            Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                              size: 20,
                            ),
                            Text(
                              ' Prev',
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Congratulation()));
                          if (_formKey.currentState.validate()) {
                            // Process data.
                          }
                        },
                        child: Row(
                          children: [
                            Text(
                              'Next ',
                              style: TextStyle(color: Colors.white),
                            ),
                            Icon(
                              Icons.arrow_forward,
                              color: Colors.white,
                              size: 20,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: Text(
                    '© eDhukutiWorldwide',
                    style: TextStyle(color: Hexcolor('#D2B78E')),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
