import 'package:edhukuti/screens/addmoneypool.dart';
import 'package:edhukuti/screens/main/home_layout.dart';
import 'package:edhukuti/widgets/landing_icon.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'Dashboard.dart';

class Login extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 50.0),
            child: Column(
              children: [
                LandingIcon(),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        TextFormField(
                          decoration: const InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.all(8),
                            prefixIcon: Icon(Icons.email,
                                color: Color.fromARGB(200, 218, 173, 107)),
                            border: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                    const Radius.circular(10))),
                            fillColor: Colors.white,
                            filled: true,
                            hintText: 'Email Address',
                            hintStyle: TextStyle(color: Colors.grey),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                        ),
                        SizedBox(height: 10),
                        TextFormField(
                          decoration: const InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.all(8),
                            prefixIcon: Icon(Icons.lock,
                                color: Color.fromARGB(200, 218, 173, 107)),
                            border: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                    const Radius.circular(10))),
                            fillColor: Colors.white,
                            filled: true,
                            hintText: 'Password',
                            suffixIcon: Icon(
                              Icons.remove_red_eye,
                              color: Color.fromARGB(200, 218, 173, 107),
                            ),
                            hintStyle: TextStyle(color: Colors.grey),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 40, bottom: 10),
                          height: 50,
                          width: 325,
                          child: RaisedButton(
                            shape: StadiumBorder(),
                            color: Hexcolor('#DAAD68'),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => HomeLayout()));
                            },
                            child: Text(
                              "Login",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                        Center(
                            child: FlatButton(
                                onPressed: null,
                                child: Text(
                                  'Forgot your password?',
                                  style: TextStyle(color: Hexcolor('#D2B78E')),
                                )))
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 80),
                Center(
                  child: Text(
                    '© eDhukutiWorldwide',
                    style: TextStyle(color: Hexcolor('#D2B78E')),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
