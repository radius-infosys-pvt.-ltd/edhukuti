import 'package:edhukuti/screens/main/home_layout.dart';
import 'package:edhukuti/screens/otpverification.dart';
import 'package:flutter/material.dart';
import '../screens/main/dashboard.dart';
import 'package:hexcolor/hexcolor.dart';

class Congratulation extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 100.0, horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Hurray!',
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        height: 2,
                        fontWeight: FontWeight.w600)),
                Text(
                    'You have been successfully registered. Please verify your profile before continuing.',
                    style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.white,
                        fontWeight: FontWeight.normal)),
                Padding(
                  padding: const EdgeInsets.only(top: 240.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      FlatButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Dashboard()));
                        },
                        child: Row(
                          children: [
                            Text(
                              ' Skip',
                              style: TextStyle(color: Colors.white),
                            ),
                            Icon(
                              Icons.skip_next,
                              color: Colors.white,
                              size: 20,
                            ),
                          ],
                        ),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HomeLayout()));
                        },
                        child: Row(
                          children: [
                            Text(
                              'Verify ',
                              style: TextStyle(color: Colors.white),
                            ),
                            Icon(
                              Icons.arrow_forward,
                              color: Colors.white,
                              size: 20,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 30),
                Center(
                  child: Text(
                    '© eDhukutiWorldwide',
                    style: TextStyle(color: Hexcolor('#D2B78E')),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
