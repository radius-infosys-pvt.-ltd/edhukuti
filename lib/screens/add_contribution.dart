import 'package:edhukuti/widgets/invitedmember.dart';
import 'package:edhukuti/widgets/my_flutter_app_icons.dart';
import 'package:edhukuti/widgets/rules.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hexcolor/hexcolor.dart';

class AddContribution extends StatefulWidget {
  @override
  _AddContributionState createState() => _AddContributionState();
}

class _AddContributionState extends State<AddContribution> {
  List<String> _timeframe = ['Daily', 'Weekly', 'Monthly', 'Yearly'];
  List<String> _budgetCategory = [
    'Housing',
    'Tavelling',
    'Entertainment',
    'Cafe',
    'Others'
  ];
  List<String> _tags = ['Camping', 'Trekking', 'Racing', 'Disco', 'Others'];

  String _selectedTimeFrame;
  String _selectedCategory;
  String _selectedTags;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          title: Text('Add Recurring Contribution'),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(12),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 16,
                ),
                TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(MyFlutterApp.wallet_1,
                        color: Color.fromARGB(200, 218, 173, 107)),
                    enabledBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                    ),
                    focusedBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                    ),

                    labelText: 'Budget Name',
                    labelStyle: TextStyle(color: Hexcolor('#ACA9A2')),
                    isDense: true,
                    // Added this
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(MyFlutterApp.pools,
                        color: Color.fromARGB(200, 218, 173, 107)),
                    enabledBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                    ),
                    focusedBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                    ),

                    labelText: 'Budget Amount',
                    labelStyle: TextStyle(color: Hexcolor('#ACA9A2')),
                    isDense: true,
                    // Added this
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                Container(
                  child: DropdownButtonFormField(
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.calendar_today,
                          color: Color.fromARGB(200, 218, 173, 107)),
                      enabledBorder: new OutlineInputBorder(
                        borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                      ),
                      focusedBorder: new OutlineInputBorder(
                        borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                      ),

                      labelText: 'Time Frame',
                      labelStyle: TextStyle(color: Hexcolor('#ACA9A2')),
                      isDense: true,
                      // Added this
                    ),
                    value: _selectedTimeFrame,
                    onChanged: (newValue) {
                      setState(() {
                        _selectedTimeFrame = newValue;
                      });
                    },
                    items: _timeframe.map((timeFrame) {
                      return DropdownMenuItem(
                        child: new Text(timeFrame),
                        value: timeFrame,
                      );
                    }).toList(),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                Container(
                  child: DropdownButtonFormField(
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.category,
                          color: Color.fromARGB(200, 218, 173, 107)),
                      enabledBorder: new OutlineInputBorder(
                        borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                      ),
                      focusedBorder: new OutlineInputBorder(
                        borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                      ),

                      labelText: 'Budget Category',
                      labelStyle: TextStyle(color: Hexcolor('#ACA9A2')),
                      isDense: true,
                      // Added this
                    ),
                    value: _selectedCategory,
                    onChanged: (newValue) {
                      setState(() {
                        _selectedCategory = newValue;
                      });
                    },
                    items: _budgetCategory.map((budgetCategory) {
                      return DropdownMenuItem(
                        child: new Text(budgetCategory),
                        value: budgetCategory,
                      );
                    }).toList(),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                Container(
                  child: DropdownButtonFormField(
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.attachment,
                          color: Color.fromARGB(200, 218, 173, 107)),
                      enabledBorder: new OutlineInputBorder(
                        borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                      ),
                      focusedBorder: new OutlineInputBorder(
                        borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                      ),

                      labelText: 'Choose Tags',
                      labelStyle: TextStyle(color: Hexcolor('#ACA9A2')),
                      isDense: true,
                      // Added this
                    ),
                    value: _selectedTags,
                    onChanged: (newValue) {
                      setState(() {
                        _selectedTags = newValue;
                      });
                    },
                    items: _tags.map((tags) {
                      return DropdownMenuItem(
                        child: new Text(tags),
                        value: tags,
                      );
                    }).toList(),
                  ),
                ),
                Container(
                      margin: EdgeInsets.only(top: 40, bottom: 10),
                      height: 50,
                      width: 325,
                      child: RaisedButton(
                        shape: StadiumBorder(),
                        color: Hexcolor('#DAAD68'),
                        onPressed: () {
                         
                        },
                        child: Text(
                          "Confirm",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
              ],
            ),
          ),
        ));
  }
}
