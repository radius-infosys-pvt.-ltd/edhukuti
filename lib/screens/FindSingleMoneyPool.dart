import 'package:edhukuti/widgets/active_pool_members.dart';
import 'package:edhukuti/widgets/circle_progress_bar.dart';
import 'package:flutter/material.dart';
import "../constants/color_constants.dart";

class FindSingleMoneyPool extends StatefulWidget {
  @override
  _FindSingleMoneyPoolState createState() => _FindSingleMoneyPoolState();
}

class _FindSingleMoneyPoolState extends State<FindSingleMoneyPool> {
  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Continue"),
      onPressed: () {},
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Join Pool"),
      content: Text("Are you sure to join this pool?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Maharjan Money Pool'),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: SafeArea(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(children: <Widget>[
              Container(
                height: 250.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(20),
                      bottomRight: Radius.circular(20)),
                  color: primaryGreen,
                ),
              ),
              Center(
                child: Column(
                  children: [
                    SizedBox(height: 10),
                    FlatButton(
                      onPressed: () {
                        showAlertDialog(context);
                      },
                      child: CircularPercentIndicator(
                        radius: 150.0,
                        lineWidth: 8.0,
                        percent: 0.0,
                        center: Icon(Icons.add, size: 50, color: Colors.grey),
                        progressColor: Colors.green,
                        backgroundColor: Colors.grey,
                      ),
                    ),
                    SizedBox(height: 5),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Pool Amount:',
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Philosopher',
                              fontSize: 20),
                        ),
                        Text(
                          ' \$80,000',
                          style: TextStyle(
                              color: Colors.white, fontFamily: 'Philosopher'),
                        ),
                      ],
                    ),
                    SizedBox(height: 5),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Active Memebers:',
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Philosopher',
                              fontSize: 20),
                        ),
                        Text(
                          ' 5',
                          style: TextStyle(
                              color: Colors.white, fontFamily: 'Philosopher'),
                        ),
                      ],
                    ),
                    SizedBox(height: 5),
                  ],
                ),
              ),
            ]),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        'Active Members',
                        style: TextStyle(
                            color: Theme.of(context).secondaryHeaderColor,
                            fontSize: 20,
                            fontFamily: 'Philosopher',
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                  ActivePoolMembers(),
                  ActivePoolMembers(),
                  ActivePoolMembers(),
                  ActivePoolMembers()
                ],
              ),
            ),
          ],
        )),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showAlertDialog(context);
        },
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        backgroundColor: primaryGreen,
      ),
    );
  }
}
