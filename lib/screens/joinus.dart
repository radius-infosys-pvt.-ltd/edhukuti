import 'package:edhukuti/screens/login.dart';
import 'package:edhukuti/screens/register.dart';
import 'package:edhukuti/widgets/landing_icon.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class JoinUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 50.0),
          child: Column(
            children: [
              LandingIcon(),
              Container(
                margin:
                    EdgeInsets.only(top: 60, left: 40, right: 40, bottom: 10),
                height: 55,
                width: 325,
                child: RaisedButton(
                  shape: StadiumBorder(),
                  color: Hexcolor('#DAAD68'),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Register()));
                  },
                  child: Text(
                    "Join Us Now",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ),
              FlatButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Login()));
                  },
                  child: Text("Already a Member?  Login Here",
                      style: TextStyle(
                          color: Hexcolor('#D2B78E'),
                          fontSize: 16,
                          fontWeight: FontWeight.w600))),
              SizedBox(height: 50),
              Text(
                '© eDhukutiWorldwide',
                style: TextStyle(color: Hexcolor('#D2B78E')),
              )
            ],
          ),
        ),
      ),
    );
  }
}
