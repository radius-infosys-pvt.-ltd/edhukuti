import 'package:edhukuti/widgets/recommendedpool.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class FindMoneyPool extends StatefulWidget {
  @override
  _FindMoneyPoolState createState() => _FindMoneyPoolState();
}

class _FindMoneyPoolState extends State<FindMoneyPool> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Find Money Pool'),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Please enter the name of the pool',
                    style: TextStyle(
                        color: Theme.of(context).secondaryHeaderColor,
                        fontWeight: FontWeight.w600,
                        fontSize: 14.0)),
                SizedBox(
                  height: 10,
                ),
                TextField(
                  decoration: InputDecoration(
                    enabledBorder: new OutlineInputBorder(
                        borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                        borderRadius: BorderRadius.circular(10)),
                    focusedBorder: new OutlineInputBorder(
                      borderSide: BorderSide(color: Hexcolor('#ACA9A2')),
                    ),

                    labelText: 'Search',
                    labelStyle: TextStyle(color: Hexcolor('#BDBDBD')),
                    isDense: true,
                    // Added this
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Icon(
                            Icons.filter_list,
                            size: 30,
                          ),
                          Text("Filter")
                        ])),
                RecommendedPool(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
