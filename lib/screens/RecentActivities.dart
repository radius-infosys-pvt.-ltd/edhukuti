import 'package:edhukuti/models/message_model.dart';
import 'package:edhukuti/screens/ChatScreen.dart';
import 'package:edhukuti/screens/SingleMoneyPools.dart';
import 'package:edhukuti/widgets/active_pool_members.dart';
import 'package:edhukuti/widgets/circle_progress_bar.dart';
import 'package:flutter/material.dart';
import '../constants/color_constants.dart';
import 'package:edhukuti/widgets/circle_progress_bar.dart';
import "ChatScreen.dart";

final startColor = Color(0xFFaa7ce4);
final endColor = Color(0xFFe46792);
final titleColor = Color(0xff444444);
final textColor = Color(0xFFa9a9a9);
final shadowColor = Color(0xffe9e9f4);

class RecentActivities extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text('Recent Activities'),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(12),
            child: ListView(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/male.png'),
                          ),
                          padding: EdgeInsets.only(
                            right: 10,
                            left: 5,
                          ),
                        ),
                        Flexible(
                          child: Text(
                              'Rojan Shrestha recently added  \$1000 in Maharaja Pool',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                              )),
                        ),
                        Text(
                          '10m',
                          style: TextStyle(
                            fontWeight: FontWeight.w300,
                            fontSize: 12,
                            color: Colors.grey[500],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Divider(
                      color: Colors.grey[400],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/profile.jpg'),
                          ),
                          padding: EdgeInsets.only(
                            right: 10,
                            left: 5,
                          ),
                        ),
                        Flexible(
                          child: Text(
                              'Rojan Shrestha recently added  \$1000 in Maharaja Pool',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                              )),
                        ),
                        Text(
                          '10m',
                          style: TextStyle(
                            fontWeight: FontWeight.w300,
                            fontSize: 12,
                            color: Colors.grey[500],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Divider(
                      color: Colors.grey[400],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/profile.jpg'),
                          ),
                          padding: EdgeInsets.only(
                            right: 10,
                            left: 5,
                          ),
                        ),
                        Flexible(
                          child: Text(
                              'Your request to join Shiva Money pool was accepted.',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                              )),
                        ),
                        Text(
                          '1h',
                          style: TextStyle(
                            fontWeight: FontWeight.w300,
                            fontSize: 12,
                            color: Colors.grey[500],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Divider(
                      color: Colors.grey[400],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
