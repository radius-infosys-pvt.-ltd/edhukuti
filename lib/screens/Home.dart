import 'package:edhukuti/screens/joinus.dart';
import 'package:edhukuti/widgets/landing_icon.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<String> _locations = ['English', 'Nepali']; // Option 2
  String _selectedLocation;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: SafeArea(
            child: Padding(
          padding: EdgeInsets.symmetric(vertical: 50.0),
          child: Column(
            children: [
              LandingIcon(),
              SizedBox(height: 50),
              Text(
                "Choose your Language",
                style: TextStyle(color: Hexcolor('#D2B78E'), fontSize: 18.0),
              ),
              Container(
                padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                margin: EdgeInsets.only(top: 10, left: 90, right: 90),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        blurRadius: 5,
                        spreadRadius: 1)
                  ],
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: DropdownButton(
                  isExpanded: true,
                  underline: SizedBox(),
                  hint: Text(_locations[0]),
                  value: _selectedLocation,
                  onChanged: (newValue) {
                    setState(() {
                      _selectedLocation = newValue;
                    });
                  },
                  items: _locations.map((location) {
                    return DropdownMenuItem(
                      child: new Text(location),
                      value: location,
                    );
                  }).toList(),
                ),
              ),
              SizedBox(height: 10),
              Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 40),
                height: 70,
                width: 220,
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(50.0)),
                child: RaisedButton(
                  shape: StadiumBorder(),
                  color: Hexcolor('#DAAD68'),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>JoinUs()));
                  },
                  child: Text(
                    'Continue',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              SizedBox(height: 20),
              Text(
                '© eDhukutiWorldwide',
                style: TextStyle(color: Hexcolor('#D2B78E')),
              )
            ],
          ),
        )));
  }
}
