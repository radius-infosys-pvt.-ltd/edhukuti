import 'package:edhukuti/widgets/fundhouse.dart';
import 'package:edhukuti/widgets/moneypool_details.dart';
import 'package:edhukuti/widgets/single_moneypool.dart';
import 'package:flutter/material.dart';

class ViewMoneyPool extends StatefulWidget {
  @override
  _MoneyState createState() => _MoneyState();
}

class _MoneyState extends State<ViewMoneyPool> {
  
   PageController _controller = PageController(
    initialPage: 1,
  );

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _controller,
      children: [
        MoneyPoolDetails(),
        SingleMoneyPools(),
        FundHouse()
      ],
    );
  }
}