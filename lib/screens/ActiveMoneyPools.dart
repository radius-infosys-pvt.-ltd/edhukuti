import 'package:edhukuti/screens/view_moneypool.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../widgets/circle_progress_bar.dart';
import '../constants/color_constants.dart';

class AddMoneyPools extends StatefulWidget {
  AddMoneyPools({Key key}) : super(key: key);

  @override
  _AddMoneyPoolsState createState() => _AddMoneyPoolsState();
}

class _AddMoneyPoolsState extends State<AddMoneyPools> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Active Money Pools'),
          actions: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Icon(Icons.search),
            ),
          ],
        ),
        body: ListView(
          scrollDirection: Axis.vertical,
          children: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ViewMoneyPool()),
                );
              },
              padding: EdgeInsets.all(0),
              child: Container(
                  height: 90,
                  width: double.infinity,
                  child: Card(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 15.0, right: 12.0),
                              child: CircularPercentIndicator(
                                radius: 60.0,
                                lineWidth: 8.0,
                                percent: 0.8,
                                center: new Text("80%"),
                                progressColor: Colors.green,
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Maharaja Money Pool',
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: goldenColor,
                                        fontFamily: "Philosopher",
                                        fontWeight: FontWeight.w600)),
                                SizedBox(height: 8),
                                Text('Pool Amount : \$80,000',
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.grey,
                                        fontWeight: FontWeight.w600)),
                              ],
                            ),
                          ],
                        ),
                        Container(
                            padding: EdgeInsets.only(right: 15),
                            child: Icon(
                              FontAwesomeIcons.check,
                              color: Colors.green,
                            ))
                      ],
                    ),
                    elevation: 2,
                  )),
            ),
            Container(
                height: 90,
                width: double.infinity,
                child: Card(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 15.0, right: 12.0),
                            child: CircularPercentIndicator(
                              radius: 60.0,
                              lineWidth: 8.0,
                              percent: 0.5,
                              center: new Text("50%"),
                              progressColor: Colors.green,
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text('Rajbansi Money Pool',
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: goldenColor,
                                      fontFamily: "Philosopher",
                                      fontWeight: FontWeight.w600)),
                              SizedBox(height: 8),
                              Text('Pool Amount : \$30,000',
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w600)),
                            ],
                          ),
                        ],
                      ),
                      Container(
                          padding: EdgeInsets.only(right: 15),
                          child: Icon(
                            FontAwesomeIcons.check,
                            color: Colors.green,
                          ))
                    ],
                  ),
                  elevation: 2,
                )),
            Container(
                height: 90,
                width: double.infinity,
                child: Card(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 15.0, right: 12.0),
                            child: CircularPercentIndicator(
                              radius: 60.0,
                              lineWidth: 8.0,
                              percent: 0.6,
                              center: new Text("60%"),
                              progressColor: Colors.red,
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text('Shree Laxmi Money Pool',
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: goldenColor,
                                      fontFamily: "Philosopher",
                                      fontWeight: FontWeight.w600)),
                              SizedBox(height: 8),
                              Text('Pool Amount : \$10,000',
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w600)),
                            ],
                          ),
                        ],
                      ),
                      Container(
                          padding: EdgeInsets.only(right: 15),
                          child: Icon(
                            FontAwesomeIcons.exclamationCircle,
                            color: Colors.red,
                          ))
                    ],
                  ),
                  elevation: 2,
                ))
          ],
        ));
  }
}
