import 'package:edhukuti/screens/Home.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import './screens/main/home_layout.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'eDhukuti',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Hexcolor("#036562"),
        accentColor: Hexcolor('#DAAD68'),
        secondaryHeaderColor: Hexcolor('#B59374'),
        fontFamily: 'Poppins',
      ),
      home: Home(),
    );
  }
}
